import fs from 'fs/promises'
import path from 'path'
import matter from 'gray-matter'
import type { Article } from '@/@types/model'

export default async function getAllArticles(): Promise<Article[]> {
  const articlesFiles = await fs.readdir(path.join(process.cwd(), 'src', 'data', 'articles'))
  const filteredArticlesFiles = articlesFiles.filter((file) => file.endsWith('.md'))

  const articles = await Promise.all(
    filteredArticlesFiles.map(async (filename) => {
      const fileContent = await fs.readFile(path.join(process.cwd(), 'src', 'data', 'articles', filename), 'utf-8')
      const { data, content } = matter(fileContent)

      return {
        metadata: data as Article['metadata'],
        slug: filename.replace('.md', ''),
        content
      }
    })
  )

  return articles
}
