import fs from 'fs/promises'
import path from 'path'
import matter from 'gray-matter'
import type { Article } from '@/@types/model'

export default async function getArticleBySlug (slug: string): Promise<Article | null> {
  try {
    const fileContent = await fs.readFile(path.join(process.cwd(), 'src', 'data', 'articles', `${slug}.md`), 'utf-8')
    const { data, content } = matter(fileContent)
  
    return {
      metadata: data as Article['metadata'],
      slug,
      content
    }
  } catch (error) {
    return null
  }
}