interface PageLayoutProps {
  children: React.ReactNode
}

export default function PageLayout ({ children }: PageLayoutProps) {
  return (
    <main className="min-h-screen flex flex-col">
      <header className="w-full max-w-6xl mx-auto flex flex-col mt-8 px-4">
        <h1 className="font-bold text-4xl mb-2">
          JavaScript en Español Plano
        </h1>
        <p>
          Blog sobre todo lo que tienes que saber de JavaScript en Español.
        </p>
        <hr className="border-dotted border-opacity-50 my-8" />
      </header>
      <div className="w-full max-w-6xl mx-auto flex flex-col flex-1">
        {children}
      </div>
      <footer className="w-full max-w-6xl mx-auto mt-8 text-center py-8">
        <small>
          © 2021 - Made with ❤️ by <a href="https://dobleb.cl" target="_blank" rel="noopener noreferrer" className="font-bold hover:underline hover:opacity-70 transition">dobleB.</a>
        </small>
      </footer>
    </main>
  )
}