import type { Article } from "@/@types/model";
import { formatDistance } from "date-fns";
import { es } from "date-fns/locale";
import Link from "next/link";

interface ArticleItemProps {
  article: Article
}

export default function ArticleItem ({ article }: ArticleItemProps) {
  return (
    <li className="group">
      <Link href={`/articles/${article.slug}`}>
        <small className="text-neutral-400 text-xs">
          Hace {formatDistance(article.metadata.date, new Date(), { locale: es })}
        </small>
        <h2 className="font-bold text-2xl group-hover:underline">{article.metadata.title}</h2>
        <p>{article.metadata.description}</p>
      </Link>
    </li>
  )
}