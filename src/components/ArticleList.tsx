import { Article } from "@/@types/model"
import ArticleItem from "./ArticleItem"

interface ArticleListProps {
  articles: Article[]
}

export default function ArticleList ({ articles }: ArticleListProps) {
  return (
    <ul className="flex flex-col gap-8">
      {articles.map((article) => (
        <ArticleItem key={article.slug} article={article} />
      ))}
    </ul>
  )
}