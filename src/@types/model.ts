export interface Article {
  metadata: {
    title: string
    description: string
    date: Date
    tags: string[]
  }
  slug: string
  content: string
}