import ArticleList from "@/components/ArticleList";
import PageLayout from "@/components/PageLayout";
import getAllArticles from "@/controllers/articles/getAllArticles";

export default async function Home() {
  const articles = await getAllArticles()

  return (
    <PageLayout>
      <ArticleList articles={articles} />
    </PageLayout>
  )
}
