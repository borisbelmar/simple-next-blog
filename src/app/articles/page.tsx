import getAllArticles from '@/controllers/articles/getAllArticles'
import PageLayout from '@/components/PageLayout'
import ArticleList from '@/components/ArticleList'

export default async function ArticlesPage() {
  const articles = await getAllArticles()

  return (
    <PageLayout>
      <ArticleList articles={articles} />
    </PageLayout>
  )
}
