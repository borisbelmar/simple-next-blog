import PageLayout from "@/components/PageLayout"
import getAllArticles from "@/controllers/articles/getAllArticles"
import getArticleBySlug from "@/controllers/articles/getArticleBySlug"
import { notFound } from "next/navigation"
import ReactMarkdown from "react-markdown"
import remarkGfm from 'remark-gfm'

interface PageProps {
  params: {
    slug: string
  }
}

export async function generateStaticParams () {
  const articles = await getAllArticles()

  return articles.map((article) => ({
    params: {
      slug: article.slug
    }
  }))
}

export default async function ArticlePage({ params }: PageProps) {
  const article = await getArticleBySlug(params.slug)

  if (!article) {
    notFound()
  }

  return (
    <PageLayout>
      <ReactMarkdown remarkPlugins={[remarkGfm]} className="prose prose-neutral prose-invert mx-auto max-w-none">
        {article.content}
      </ReactMarkdown>
    </PageLayout>
  )
}
