import './globals.css'
import type { Metadata } from 'next'
import { Inter } from 'next/font/google'

const inter = Inter({ subsets: ['latin'] })

export const metadata: Metadata = {
  title: 'JavaScript en Español Plano',
  description: 'Blog sobre todo lo que tienes que saber de JavaScript'
}

interface RootLayoutProps {
  children: React.ReactNode
}

export default function RootLayout({ children }: RootLayoutProps) {
  return (
    <html lang="es">
      <body className={`${inter.className} bg-neutral-800 text-white`}>{children}</body>
    </html>
  )
}
